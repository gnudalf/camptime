from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m import logging
from st3m.goose import Optional
from ctx import Context
import leds

import math
import random
import st3m.run
import ntptime
import network
import time

log = logging.Log(__name__, level=logging.INFO)
log.info("time")

TIMEZONE = 2 # this is camp, here and now we are +2 hours to UTC

def initialize_wlan(ssid="Camp2023-open"):
    # TODO not the best way to do this?
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    try:
        wlan.connect(ssid)
    except Exception as e:
        log.error(f"initialize_wlan failed")
        log.error(e)
        return
    log.info(f"initialize_wlan: connected {wlan.isconnected()}")
    if wlan.isconnected():
        log.info(wlan.ifconfig())
    return wlan

class Time(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._time_str = "00:00:00"
        self.ntp_synced = False
        self.init_ntp()

    def on_enter(self, vm: Optional["ViewManager"]) -> None:
        self.init_ntp()

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 80
        ctx.font = ctx.get_font_name(5)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.rgb(1,1,1)
        ctx.move_to(0, 0)
        ctx.save()
        ctx.text(self._time_str)
        ctx.restore()
        # TODO draw analog clock instead

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self._time = t = time.localtime(time.time() + TIMEZONE*3600)
        # TODO no rjust in micropython?
        self._time_str = ":".join(map(lambda x: str(x) if x >= 10 else f"0{x}", t[3:6]))


    def init_ntp(self) -> None:
        if self.ntp_synced:
            return
        wlan = initialize_wlan()
        if not wlan:
            return
        time.sleep(2)
        if wlan.isconnected():
            log.info(f"setup ntp")
            # TODO.... ntptime raises ETIMEDOUT but seems to work nonetheless
            try:
                ntptime.settime()
            except Exception as e:
                log.error(e)
            self.ntp_synced = True
            wlan.disconnect()


if __name__ == '__main__':
    st3m.run.run_view(Time(ApplicationContext()))
